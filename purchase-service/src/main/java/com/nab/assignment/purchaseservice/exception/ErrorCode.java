package com.nab.assignment.purchaseservice.exception;

public enum ErrorCode {
  ERROR_VALIDATING_REQUEST("Error validating the request"),
  ERROR_INTERNAL_SERVICE_REQUEST("Error internal %s service request");

  private String message;

  ErrorCode(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
