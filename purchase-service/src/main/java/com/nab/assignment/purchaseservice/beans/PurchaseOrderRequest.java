package com.nab.assignment.purchaseservice.beans;

import com.nab.assignment.purchaseservice.validator.ValidRequest;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Data
@ValidRequest
public class PurchaseOrderRequest implements Serializable {
  private static final long serialVersionUID = 5398254779477376977L;
  @NotNull
  private Customer customer;
  @NotNull
  private List<Product> products;
}
