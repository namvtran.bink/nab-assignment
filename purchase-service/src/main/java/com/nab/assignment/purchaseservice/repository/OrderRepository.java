package com.nab.assignment.purchaseservice.repository;

import com.nab.assignment.purchaseservice.entity.PurchaseOrder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<PurchaseOrder, Integer>, JpaSpecificationExecutor<PurchaseOrder> {

  @Override
  Optional<PurchaseOrder> findById(Integer aLong);

  @Override
  List<PurchaseOrder> findAll(Specification<PurchaseOrder> specification);

  @Override
  <S extends PurchaseOrder> S save(S s);

  @Override
  void deleteById(Integer aLong);
}
