package com.nab.assignment.purchaseservice.proxy;

import com.nab.assignment.purchaseservice.beans.Customer;

public interface CustomerServiceProxy {
  int getCustomerId(Customer customer);
}
