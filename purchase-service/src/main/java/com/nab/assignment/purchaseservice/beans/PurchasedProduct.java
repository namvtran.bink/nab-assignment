package com.nab.assignment.purchaseservice.beans;

import lombok.Data;

@Data
public class PurchasedProduct {
  private int productId;
  private int quantity;
  OrderStatus orderStatus;
}
