package com.nab.assignment.purchaseservice.builder;

import com.nab.assignment.purchaseservice.beans.OrderSpecification;
import com.nab.assignment.purchaseservice.beans.SearchCriteria;
import com.nab.assignment.purchaseservice.entity.PurchaseOrder;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderSpecificationBuilder {

  private final List<SearchCriteria> params;

  public OrderSpecificationBuilder() {
    params = new ArrayList<>();
  }

  public OrderSpecificationBuilder with(String key, String operation, Object value) {
    params.add(new SearchCriteria(key, operation, value));
    return this;
  }

  public Specification<PurchaseOrder> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification> specs = params.stream()
        .map(OrderSpecification::new)
        .collect(Collectors.toList());

    Specification result = specs.get(0);

    for (int i = 1; i < params.size(); i++) {
      result = params.get(i)
          .isOrPredicate()
          ? Specification.where(result)
          .or(specs.get(i))
          : Specification.where(result)
          .and(specs.get(i));
    }
    return result;
  }
}
