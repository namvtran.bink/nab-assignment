package com.nab.assignment.purchaseservice.exception;

public class InternalServiceRequestException extends RuntimeException {
  public InternalServiceRequestException() {
    super();
  }


  public InternalServiceRequestException(ErrorCode errorCode,  String serviceName) {
    super(String.format(errorCode.getMessage(), serviceName));
  }

  public InternalServiceRequestException(ErrorCode errorCode,  String serviceName,Exception ex) {
    super(String.format(errorCode.getMessage(), serviceName), ex);
  }
}
