package com.nab.assignment.purchaseservice.controller;

import com.nab.assignment.purchaseservice.beans.PurchaseOrderRequest;
import com.nab.assignment.purchaseservice.entity.PurchaseOrder;
import com.nab.assignment.purchaseservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/Orders")
public class OrderController {

  @Autowired
  private OrderService orderService;

  @GetMapping("/{id}")
  public PurchaseOrder getOrder(@PathVariable int id) {
    return orderService.getOrder(id);
  }

  @GetMapping
  @ResponseBody
  public List<PurchaseOrder> getOrders(@RequestParam(required = false) String search,
                                       @RequestParam(required = false) String sort,
                                       @RequestParam(required = false) Sort.Direction direction) {
    return orderService.getOrders(search, sort, direction);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public void createOrder(@Valid @RequestBody PurchaseOrderRequest purchaseOrderRequest) {
    orderService.createOrder(purchaseOrderRequest);
  }

//  @PutMapping("/{id}")
//  public void updateOrder(@PathVariable int id,
//                            @Valid @RequestBody PurchaseOrderRequest createOrderRequest) {
//    orderService.updateOrder(id, createOrderRequest);
//  }

  @DeleteMapping("/{id}")
  public void deleteOrder(@PathVariable int id) {
    orderService.deleteOrder(id);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(HttpClientErrorException.BadRequest.class)
  public Map<String, String> handleValidationExceptions(
      HttpClientErrorException.BadRequest ex) {
    Map<String, String> errors = new HashMap<>();
    errors.put("message", ex.getMessage());
    return errors;
  }
}
