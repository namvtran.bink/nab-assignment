package com.nab.assignment.purchaseservice.beans;

import lombok.Data;

@Data
public class Product {
  private int productId;
  private int quantity;
}
