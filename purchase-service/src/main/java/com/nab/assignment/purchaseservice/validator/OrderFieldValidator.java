package com.nab.assignment.purchaseservice.validator;

import com.nab.assignment.purchaseservice.entity.PurchaseOrder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

@Component
public class OrderFieldValidator {

  public void validate(String fieldName) {
    try {
      PurchaseOrder.class.getDeclaredField(fieldName);
    } catch (NoSuchFieldException e) {
      throw HttpClientErrorException.create("PurchaseOrder not have field name: " + e.getMessage(), HttpStatus.BAD_REQUEST,
          HttpStatus.BAD_REQUEST.getReasonPhrase(), null, null, null);
    }
  }
}
