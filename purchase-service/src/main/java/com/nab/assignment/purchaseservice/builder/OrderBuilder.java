package com.nab.assignment.purchaseservice.builder;

import com.nab.assignment.purchaseservice.beans.OrderStatus;
import com.nab.assignment.purchaseservice.entity.PurchaseOrder;

public class OrderBuilder {
  private PurchaseOrder purchaseOrder;
  
  public OrderBuilder() {
    purchaseOrder = new PurchaseOrder();
  }


  public OrderBuilder(PurchaseOrder purchaseOrder) {
    this.purchaseOrder = purchaseOrder;
  }

  public OrderBuilder setId(int id) {
    purchaseOrder.setId(id);
    return this;
  }

  public OrderBuilder setCustomerId(int customerId) {
    purchaseOrder.setCustomerId(customerId);
    return this;
  }

  public OrderBuilder setProductId(int productId) {
    purchaseOrder.setProductId(productId);
    return this;
  }

  public OrderBuilder setQuantity(int quantity) {
    purchaseOrder.setQuantity(quantity);
    return this;
  }

  public OrderBuilder setOrderStatus(OrderStatus status) {
    purchaseOrder.setStatus(status);
    return this;
  }
  
  public PurchaseOrder build() {
    return purchaseOrder;
  }
}
