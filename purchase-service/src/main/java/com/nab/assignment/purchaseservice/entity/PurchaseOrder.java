package com.nab.assignment.purchaseservice.entity;

import com.nab.assignment.purchaseservice.beans.OrderStatus;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class PurchaseOrder {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private int customerId;
  private int productId;
  private int quantity;
  private OrderStatus status;
}
