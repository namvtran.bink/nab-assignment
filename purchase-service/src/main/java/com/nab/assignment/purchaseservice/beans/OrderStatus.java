package com.nab.assignment.purchaseservice.beans;

public enum OrderStatus {
  SUCCESSFUL,
  FAILED,
  PURCHASING;
}
