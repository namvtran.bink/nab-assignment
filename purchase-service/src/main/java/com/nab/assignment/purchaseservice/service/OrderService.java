package com.nab.assignment.purchaseservice.service;

import com.nab.assignment.purchaseservice.beans.PurchaseOrderRequest;
import com.nab.assignment.purchaseservice.entity.PurchaseOrder;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface OrderService {

  void createOrder(PurchaseOrderRequest purchaseOrderRequest);

  PurchaseOrder getOrder(int id);

  List<PurchaseOrder> getOrders(String search, String sort, Sort.Direction direction);

  void deleteOrder(int id);

}
