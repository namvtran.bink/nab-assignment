package com.nab.assignment.purchaseservice.beans;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
public class Customer {
  private Integer customerId;
  @NotEmpty
  private String name;
  @NotEmpty
  private String phoneNumber;
  @Email
  @NotEmpty
  private String email;
  @NotEmpty
  private String address;
}
