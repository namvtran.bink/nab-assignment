package com.nab.assignment.purchaseservice.beans;

import lombok.Data;

import java.util.List;

@Data
public class RepositoryResponse {
  List<PurchasedProduct> purchasedProducts;
}
