package com.nab.assignment.purchaseservice.service.impl;

import com.nab.assignment.purchaseservice.beans.PurchaseOrderRequest;
import com.nab.assignment.purchaseservice.beans.PurchasedProduct;
import com.nab.assignment.purchaseservice.builder.OrderBuilder;
import com.nab.assignment.purchaseservice.builder.OrderSpecificationBuilder;
import com.nab.assignment.purchaseservice.entity.PurchaseOrder;
import com.nab.assignment.purchaseservice.proxy.CustomerServiceProxy;
import com.nab.assignment.purchaseservice.proxy.RepositoryServiceProxy;
import com.nab.assignment.purchaseservice.repository.OrderRepository;
import com.nab.assignment.purchaseservice.service.OrderService;
import com.nab.assignment.purchaseservice.validator.OrderFieldValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class OrderServiceImpl implements OrderService {
  private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private OrderFieldValidator orderFieldValidator;

  @Autowired
  private CustomerServiceProxy customerServiceProxy;

  @Autowired
  private RepositoryServiceProxy repositoryServiceProxy;

  @Override
  @Transactional(rollbackOn = Exception.class)
  public void createOrder(PurchaseOrderRequest purchaseOrderRequest) {
    logger.info("Start purchase order of {}", purchaseOrderRequest.getCustomer().toString());
      // getCustomerId from customer service by provided info
      int customerId = customerServiceProxy.getCustomerId(purchaseOrderRequest.getCustomer());

      // purchase product with productId and quantity
      List<PurchasedProduct> purchasedProducts = repositoryServiceProxy.purchaseProducts(
          purchaseOrderRequest.getProducts());

    try {
      // save record to db
      savePurchasedOrder(customerId, purchasedProducts);
    } catch (Exception ex) {
      // unpurchase order if any exception
      repositoryServiceProxy.unpurchaseProducts(purchaseOrderRequest.getProducts());
      throw ex;
    }
  }

  private void savePurchasedOrder(int customerId, List<PurchasedProduct> purchasedProducts) {
    List<PurchaseOrder> purchaseOrders = new ArrayList<>();
    for (PurchasedProduct product : purchasedProducts) {
      PurchaseOrder purchaseOrder = new OrderBuilder()
          .setProductId(product.getProductId())
          .setCustomerId(customerId)
          .setOrderStatus(product.getOrderStatus())
          .setQuantity(product.getQuantity())
          .build();
      purchaseOrders.add(purchaseOrder);
    }
    orderRepository.saveAll(purchaseOrders);
  }

  @Override
  public PurchaseOrder getOrder(int id) {
    return orderRepository.findById(id).orElse(null);
  }

  @Override
  public List<PurchaseOrder> getOrders(String search, String sortField, Sort.Direction direction) {
    sortField = Optional.ofNullable(sortField).orElse("name");
    orderFieldValidator.validate(sortField);

    direction = Optional.ofNullable(direction).orElse(Sort.Direction.ASC);

    Specification<PurchaseOrder> spec = getOrderSpecification(search);

    return orderRepository.findAll(spec, Sort.by(direction, sortField));
  }

  @Override
  public void deleteOrder(int id) {
    orderRepository.deleteById(id);
  }

  private Specification<PurchaseOrder> getOrderSpecification(String search) {
    OrderSpecificationBuilder builder = new OrderSpecificationBuilder();
    Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
    Matcher matcher = pattern.matcher(search + ",");
    while (matcher.find()) {
      orderFieldValidator.validate(matcher.group(1));
      builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
    }

    return builder.build();
  }
}
