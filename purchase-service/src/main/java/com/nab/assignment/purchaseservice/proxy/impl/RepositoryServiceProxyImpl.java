package com.nab.assignment.purchaseservice.proxy.impl;

import com.nab.assignment.purchaseservice.beans.Product;
import com.nab.assignment.purchaseservice.beans.PurchasedProduct;
import com.nab.assignment.purchaseservice.beans.RepositoryResponse;
import com.nab.assignment.purchaseservice.exception.ErrorCode;
import com.nab.assignment.purchaseservice.exception.InternalServiceRequestException;
import com.nab.assignment.purchaseservice.proxy.RepositoryServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Repository
public class RepositoryServiceProxyImpl implements RepositoryServiceProxy {
  private static final String REPOSITORY_SERVICE = "Repository Service";

  @Value("${nab.repositoryService.url")
  private String repositoryURL;
  @Value("${nab.repositoryService.purchasePath")
  private String purchasePath;
  @Value("${nab.repositoryService.unpurchasePath")
  private String unpurchasePath;

  @Autowired
  private RestTemplate restTemplate;

  @Override
  public List<PurchasedProduct> purchaseProducts(List<Product> products) {
    try {
      ResponseEntity<RepositoryResponse> responseEntity = restTemplate.postForEntity(
          buildServiceEndpoint(repositoryURL, purchasePath),
          products, RepositoryResponse.class);
      return handleResponse(responseEntity);
    } catch (HttpServerErrorException ex) {
      throw new InternalServiceRequestException(ErrorCode.ERROR_INTERNAL_SERVICE_REQUEST,
          REPOSITORY_SERVICE, ex);
    }
  }

  @Override
  public void unpurchaseProducts(List<Product> products) {
    try {
      restTemplate.postForEntity(
          buildServiceEndpoint(repositoryURL, purchasePath),
          products, RepositoryResponse.class);
    } catch (HttpServerErrorException ex) {
      throw new InternalServiceRequestException(ErrorCode.ERROR_INTERNAL_SERVICE_REQUEST,
          REPOSITORY_SERVICE, ex);
    }
  }

  private List<PurchasedProduct> handleResponse(ResponseEntity<RepositoryResponse> responseEntity) {
    if (responseEntity.getBody() != null
        && !CollectionUtils.isEmpty(responseEntity.getBody().getPurchasedProducts())) {
      return responseEntity.getBody().getPurchasedProducts();
    }
    throw new InternalServiceRequestException(ErrorCode.ERROR_INTERNAL_SERVICE_REQUEST,
        REPOSITORY_SERVICE);
  }

  private String buildServiceEndpoint(String url, String path) {
    return url + path;
  }
}
