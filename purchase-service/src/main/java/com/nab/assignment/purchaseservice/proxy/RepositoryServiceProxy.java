package com.nab.assignment.purchaseservice.proxy;

import com.nab.assignment.purchaseservice.beans.Product;
import com.nab.assignment.purchaseservice.beans.PurchasedProduct;

import java.util.List;

public interface RepositoryServiceProxy {
  List<PurchasedProduct> purchaseProducts(List<Product> products);

  void unpurchaseProducts(List<Product> products);
}
