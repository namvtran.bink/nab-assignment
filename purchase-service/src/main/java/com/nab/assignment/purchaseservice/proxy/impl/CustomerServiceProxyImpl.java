package com.nab.assignment.purchaseservice.proxy.impl;

import com.nab.assignment.purchaseservice.beans.Customer;
import com.nab.assignment.purchaseservice.exception.ErrorCode;
import com.nab.assignment.purchaseservice.exception.InternalServiceRequestException;
import com.nab.assignment.purchaseservice.proxy.CustomerServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Repository
public class CustomerServiceProxyImpl implements CustomerServiceProxy {
  private static final String CUSTOMER_SERVICE = "Customer Service";

  @Value("${nab.customerService.url")
  private String customerServiceUrl;
  @Value("${nab.repositoryService.queryPath")
  private String queryPath;

  @Autowired
  private RestTemplate restTemplate;

  @Override
  public int getCustomerId(Customer customer) {
    try {
      String search = "search=name:" + customer.getName()
          + "phoneNumber:" + customer.getPhoneNumber()
          + "email:" + customer.getName()
          + "address:" + customer.getAddress();

      ResponseEntity<Customer> responseEntity = restTemplate.getForEntity(
          buildServiceEndpoint(customerServiceUrl, queryPath), Customer.class, search);
      return handleResponse(responseEntity);
    } catch (HttpServerErrorException ex) {
      throw new InternalServiceRequestException(ErrorCode.ERROR_INTERNAL_SERVICE_REQUEST,
          CUSTOMER_SERVICE, ex);
    }
  }

  private int handleResponse(ResponseEntity<Customer> responseEntity) {
    if (responseEntity.getBody() != null
        && responseEntity.getBody().getCustomerId() != null) {
      return responseEntity.getBody().getCustomerId();
    }
    throw new InternalServiceRequestException(ErrorCode.ERROR_INTERNAL_SERVICE_REQUEST,
        CUSTOMER_SERVICE);
  }

  private String buildServiceEndpoint(String url, String path) {
    return url + path;
  }
}
