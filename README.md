# nab-assignment

## Services
 - customer-service: service to get update create delete customer
 - purchase-service: service to purchase order
 - repository-service: service to get update create delete product
## Used libraries
 - spring-boot-starter-web: help provide web api rest service in project
 - spring-boot-starter-data-jpa: help provide jpa repository and hibernate api to interact with database
 - spring-boot-starter-web: help provide web api rest service in project
 - spring-boot-starter-web: help provide web api rest service in project
 - lombok: help auto generate getter setter and constructors methods
 - h2database: create embeded in memory database to project
 - javax.validation: help validate request and entity in project
 - liquibase: help to create and migrate database
 - springdoc-openapi-ui: help to create api docs
## Steps to run application in local
 - Clone this repository to your local machine with git or down load zip file and extract to your local machine.
 - Setup Java 11 to your local machine, refer to [this document](https://docs.plm.automation.siemens.com/content/polarion/20/help/en_US/polarion_windows_installation/before_installation/install_openjdk_11.html)
 - Setup maven to your local machine, refer to [this document](https://maven.apache.org/install.html)
 - Open terminal (on Linux and MacOS) or command prompt (on windows)
 - cd to the directory of this repo.
 - cd to each service directory
 - call `mvn spring-boot:run` 
## Useful Infomation
 - repository service: 
    - port: 8080
    - context path: /nab/repository-service
 - customer service: 
    - port: 8180
    - context path: /nab/customer-service
 - purchase service: 
    - port: 8280
    - context path: /nab/purchase-service
 - API docs for all service will serve in context path /v3/api-docs. Example: Api docs of repository service is localhost:8080/nab/repository-service/v3/api-docs/
 - To create update delete product you need basic authentication with admin user
   - username: admin
   - password: password
 - A business to check the remaining quantity of product presents in function:
   - com.nab.assignment.repositoryservice.service.impl.ProductServiceImpl.checkQuantityAndPurchase
 - Example request to query products filter field and sort
```
curl --location --request GET 'http://localhost:8080/nab/repository-service/products?search=name:iPhone,price%3E1000&sort=quantity&direction=DESC'
```
 
 


