package com.nab.assignment.repositoryservice.service.impl;

import com.nab.assignment.repositoryservice.beans.ChangeProductRequest;
import com.nab.assignment.repositoryservice.builder.ProductBuilder;
import com.nab.assignment.repositoryservice.entity.Product;
import com.nab.assignment.repositoryservice.repository.ProductRepository;
import com.nab.assignment.repositoryservice.validator.ProductFieldValidator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

  @Mock
  private ProductRepository productRepository;

  @Mock
  private ProductFieldValidator productFieldValidator;

  @InjectMocks
  private ProductServiceImpl productService;

  @Test
  public void testCreateProduct() {
    ChangeProductRequest changeProductRequest = new ChangeProductRequest();
    changeProductRequest.setName("Test Product");
    changeProductRequest.setDescription("description");
    changeProductRequest.setPrice(100);

    productService.createProduct(changeProductRequest);

    ArgumentCaptor<Product> argumentCaptor = ArgumentCaptor.forClass(Product.class);

    verify(productRepository).save(argumentCaptor.capture());

    Product product = argumentCaptor.getValue();

    Assert.assertNotNull(product);
    Assert.assertEquals("Test Product", product.getName());
    Assert.assertEquals("description", product.getDescription());
    Assert.assertEquals(100, product.getPrice());
  }

  @Test
  public void testGetProductWithExistProduct() {
    int id = 0;

    Product product = new Product();
    product.setId(id);
    product.setName("Test Product");
    product.setDescription("description");
    product.setPrice(100);

    when(productRepository.findById(id)).thenReturn(java.util.Optional.of(product));

    Product result = productService.getProduct(id);

    Assert.assertNotNull(result);
    Assert.assertEquals("Test Product", result.getName());
    Assert.assertEquals("description", result.getDescription());
    Assert.assertEquals(100, result.getPrice());
  }

  @Test
  public void testGetProductWithNotExistProduct() {
    int id = 0;

    when(productRepository.findById(id)).thenReturn(java.util.Optional.empty());

    Product result = productService.getProduct(id);

    Assert.assertNull(result);
  }


  @Test
  public void testGetProducts() {
    List<Product> expected = new ArrayList<>();
    expected.add(new ProductBuilder().setName("Test Product 1").build());
    expected.add(new ProductBuilder().setName("Test Product 2").build());

    when(productRepository.findAll((Specification<Product>) eq(null), any(Sort.class))).thenReturn(expected);

    List<Product> result = productService.getProducts(null, null, null);

    Assert.assertNotNull(result);
    Assert.assertEquals(expected.get(0), result.get(0));
    Assert.assertEquals(expected.get(1), result.get(1));
  }

  @Test
  public void testGetProductsWithSearchAndSort() {
    List<Product> expected = new ArrayList<>();
    expected.add(new ProductBuilder().setName("Test Product 1").build());
    expected.add(new ProductBuilder().setName("Test Product 2").build());

    when(productRepository.findAll(any(Specification.class), any(Sort.class))).thenReturn(expected);

    List<Product> result = productService.getProducts("name:Test", "description", Sort.Direction.DESC);

    Assert.assertNotNull(result);
    Assert.assertEquals(expected.get(0), result.get(0));
    Assert.assertEquals(expected.get(1), result.get(1));
  }


}