package com.nab.assignment.repositoryservice.beans;

public enum OrderStatus {
  SUCCESSFUL,
  FAILED,
  PURCHASING;
}
