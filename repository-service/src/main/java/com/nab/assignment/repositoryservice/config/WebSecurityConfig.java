package com.nab.assignment.repositoryservice.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.httpBasic()
        .and()
        .authorizeRequests()
        .antMatchers("/swagger-ui").permitAll()
        .antMatchers(HttpMethod.POST, "/products").hasRole("ADMIN")
        .antMatchers(HttpMethod.PUT, "/products/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.DELETE, "/products/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.GET,"/products/**").permitAll()
        .and()
        .csrf().disable()
        .formLogin().disable();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.inMemoryAuthentication()
        .withUser("admin").password("{noop}password").roles("ADMIN")
        .and()
        .withUser("user").password("{noop}password").roles("USER");
  }
}
