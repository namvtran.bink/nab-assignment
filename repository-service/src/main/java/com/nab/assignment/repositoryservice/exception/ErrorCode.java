package com.nab.assignment.repositoryservice.exception;

public enum ErrorCode {
  ERROR_VALIDATING_REQUEST("Error validating the request");

  private String message;

  ErrorCode(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
