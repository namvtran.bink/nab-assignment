package com.nab.assignment.repositoryservice.validator;

import com.nab.assignment.repositoryservice.entity.Product;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

@Component
public class ProductFieldValidator {

  public void validate(String fieldName) {
    try {
      Product.class.getDeclaredField(fieldName);
    } catch (NoSuchFieldException e) {
      throw HttpClientErrorException.create("Product not have field name: " + e.getMessage(), HttpStatus.BAD_REQUEST,
          HttpStatus.BAD_REQUEST.getReasonPhrase(), null, null, null);
    }
  }
}
