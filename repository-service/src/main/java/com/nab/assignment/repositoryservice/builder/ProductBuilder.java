package com.nab.assignment.repositoryservice.builder;

import com.nab.assignment.repositoryservice.entity.Product;

public class ProductBuilder {
  private Product product;
  
  public ProductBuilder() {
    product = new Product();
  }


  public ProductBuilder(Product product) {
    this.product = product;
  }

  public ProductBuilder setId(int id) {
    product.setId(id);
    return this;
  }

  public ProductBuilder setBrand(String brand) {
    product.setBrand(brand);
    return this;
  }

  public ProductBuilder setImageUrl(String imageUrl) {
    product.setImageUrl(imageUrl);
    return this;
  }

  public ProductBuilder setName(String name) {
    product.setName(name);
    return this;
  }

  public ProductBuilder setPrice(int price) {
    product.setPrice(price);
    return this;
  }

  public ProductBuilder setDescription(String description) {
    product.setDescription(description);
    return this;
  }

  public ProductBuilder setQuantity(int quantity) {
    product.setQuantity(quantity);
    return this;
  }
  
  public Product build() {
    return product;
  }
}
