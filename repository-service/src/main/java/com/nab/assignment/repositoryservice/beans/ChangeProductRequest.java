package com.nab.assignment.repositoryservice.beans;

import com.nab.assignment.repositoryservice.validator.ValidRequest;
import java.io.Serializable;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import lombok.Data;

@Data
@ValidRequest
public class ChangeProductRequest implements Serializable {
  private static final long serialVersionUID = 5398254779477376977L;

  @NotEmpty
  private String name;
  private String description;
  private int price;
  private String brand;
  @Pattern(regexp = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
  private String imageUrl;
  private int quantity;
}
