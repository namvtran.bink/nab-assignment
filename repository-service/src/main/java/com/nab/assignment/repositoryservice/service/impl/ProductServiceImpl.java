package com.nab.assignment.repositoryservice.service.impl;

import com.nab.assignment.repositoryservice.beans.OrderStatus;
import com.nab.assignment.repositoryservice.beans.PurchasedProduct;
import com.nab.assignment.repositoryservice.builder.ProductBuilder;
import com.nab.assignment.repositoryservice.beans.ChangeProductRequest;
import com.nab.assignment.repositoryservice.builder.ProductSpecificationBuilder;
import com.nab.assignment.repositoryservice.entity.Product;
import com.nab.assignment.repositoryservice.repository.ProductRepository;
import com.nab.assignment.repositoryservice.service.ProductService;
import com.nab.assignment.repositoryservice.validator.ProductFieldValidator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepository productRepository;

  @Autowired
  private ProductFieldValidator productFieldValidator;

  @Override
  public void createProduct(ChangeProductRequest changeProductRequest) {
    Product product = buildProduct(new ProductBuilder(), changeProductRequest);
    productRepository.save(product);
  }

  @Override
  public Product getProduct(int id) {
    return productRepository.findById(id).orElse(null);
  }

  @Override
  public List<Product> getProducts(String search, String sortField, Sort.Direction direction) {
    sortField = !StringUtils.isEmpty(sortField) ? sortField : "name";
    productFieldValidator.validate(sortField);
    direction = Optional.ofNullable(direction).orElse(Sort.Direction.ASC);

    Specification<Product> spec = getProductSpecification(search);

    return productRepository.findAll(spec, Sort.by(direction, sortField));
  }

  @Override
  public void updateProduct(int id, ChangeProductRequest changeProductRequest) {
    Product product = productRepository.findById(id).orElseThrow(() -> {
      String message = String.format("Product with id: %d is not exists to update", id);
      return HttpClientErrorException.create(message, HttpStatus.BAD_REQUEST,
          HttpStatus.BAD_REQUEST.getReasonPhrase(), null, null, null);
    });
    product = buildProduct(new ProductBuilder(product), changeProductRequest);
    productRepository.save(product);
  }

  @Override
  public void deleteProduct(int id) {
    productRepository.deleteById(id);
  }

  @Override
  @Transactional(rollbackOn = Exception.class)
  public List<PurchasedProduct> purchaseProduct(List<Product> products) {
    List<PurchasedProduct> purchasedProducts = new ArrayList<>();
    List<Product> needUpdateProducts = new ArrayList<>();
    for (Product purchasingProduct: products) {
      PurchasedProduct purchasedProduct = new PurchasedProduct(purchasingProduct.getId(),
          purchasingProduct.getQuantity());
      Optional<Product> optionalProduct = productRepository.findById(purchasingProduct.getId());
      if (optionalProduct.isEmpty()) {
        purchasedProduct.setOrderStatus(OrderStatus.FAILED);
      } else {
        checkQuantityAndPurchase(needUpdateProducts, purchasingProduct, purchasedProduct, optionalProduct);
      }

      purchasedProducts.add(purchasedProduct);
    }

    productRepository.saveAll(needUpdateProducts);
    return purchasedProducts;
  }

  private void checkQuantityAndPurchase(List<Product> needUpdateProducts, Product purchasingProduct, PurchasedProduct purchasedProduct, Optional<Product> optionalProduct) {
    Product product = optionalProduct.get();
    int purchasedQuantity = product.getQuantity() - purchasingProduct.getQuantity();
    if (purchasedQuantity >= 0) {
      product.setQuantity(purchasedQuantity);
      needUpdateProducts.add(product);
      purchasedProduct.setOrderStatus(OrderStatus.SUCCESSFUL);
    } else {
      purchasedProduct.setOrderStatus(OrderStatus.FAILED);
    }
  }

  @Override
  @Transactional(rollbackOn = Exception.class)
  public void  unpurchaseProduct(List<PurchasedProduct> products) {
    List<Integer> productIds = products.stream().map(PurchasedProduct::getId).collect(Collectors.toList());

    Map<Integer, Integer> productIdQuantityMap = products.stream()
        .filter(purchasedProduct -> OrderStatus.SUCCESSFUL.equals(purchasedProduct.getOrderStatus()))
        .collect(Collectors.toMap(PurchasedProduct::getId, PurchasedProduct::getQuantity));

    List<Product> currentProducts = productRepository.findAllById(productIds);
    for (Product product: currentProducts) {
      Optional<Integer> optionalProduct = Optional.ofNullable(productIdQuantityMap.get(product.getId()));
      optionalProduct.ifPresent(integer -> product.setQuantity(product.getQuantity() + integer));
    }

    productRepository.saveAll(currentProducts);
  }

  private Specification<Product> getProductSpecification(String search) {
    ProductSpecificationBuilder builder = new ProductSpecificationBuilder();
    Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
    Matcher matcher = pattern.matcher(search + ",");
    while (matcher.find()) {
      productFieldValidator.validate(matcher.group(1));
      builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
    }

    return builder.build();
  }

  private Product buildProduct(ProductBuilder productBuilder, ChangeProductRequest changeProductRequest) {
    return productBuilder
        .setName(changeProductRequest.getName())
        .setBrand(changeProductRequest.getBrand())
        .setDescription(changeProductRequest.getDescription())
        .setImageUrl(changeProductRequest.getImageUrl())
        .setPrice(changeProductRequest.getPrice())
        .setQuantity(changeProductRequest.getQuantity())
        .build();
  }
}
