package com.nab.assignment.repositoryservice.config;

import liquibase.integration.spring.SpringLiquibase;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.util.Map;

@Configuration
@EnableScheduling
public class LiquibaseConfig {

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Autowired
  private ResourceLoader resourceLoader;

  @Value("${spring.liquibase.default-schema}")
  private String defaultSchema;

  @PostConstruct
  public void applyLiquibaseForCustomers() {
    applyLiquibaseForSchema(defaultSchema);
  }

  @SneakyThrows
  private void applyLiquibaseForSchema(String schema) {
    jdbcTemplate.execute("CREATE SCHEMA IF NOT EXISTS \"" + schema + "\"");
    SpringLiquibase liquibase = new SpringLiquibase();
    liquibase.setResourceLoader(resourceLoader);
    liquibase.setDataSource(jdbcTemplate.getDataSource());
    liquibase.setDefaultSchema(schema);
    liquibase.setChangeLog("classpath:db/changelog/db.changelog-master.xml");
    liquibase.setChangeLogParameters(Map.of("schema", schema));
    liquibase.afterPropertiesSet();
  }

}
