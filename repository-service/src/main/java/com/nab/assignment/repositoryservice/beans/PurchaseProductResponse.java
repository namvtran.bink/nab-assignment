package com.nab.assignment.repositoryservice.beans;

import lombok.Data;

import java.util.List;

@Data
public class PurchaseProductResponse {
  List<PurchasedProduct> products;
}
