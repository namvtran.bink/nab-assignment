package com.nab.assignment.repositoryservice.controller;

import com.nab.assignment.repositoryservice.beans.ChangeProductRequest;
import com.nab.assignment.repositoryservice.beans.PurchaseProductResponse;
import com.nab.assignment.repositoryservice.beans.PurchasedProduct;
import com.nab.assignment.repositoryservice.entity.Product;
import com.nab.assignment.repositoryservice.service.ProductService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/products")
public class ProductController {

  @Autowired
  private ProductService productService;

  @GetMapping("/{id}")
  public Product getProduct(@PathVariable int id) {
    return productService.getProduct(id);
  }

  @GetMapping
  @ResponseBody
  public List<Product> getProducts(@RequestParam(required = false) String search,
                                   @RequestParam(required = false) String sort,
                                   @RequestParam(required = false) Sort.Direction direction) {
    return productService.getProducts(search, sort, direction);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public void createProduct(@Valid @RequestBody ChangeProductRequest changeProductRequest) {
    productService.createProduct(changeProductRequest);
  }

  @PutMapping("/{id}")
  public void updateProduct(@PathVariable int id,
                            @Valid @RequestBody ChangeProductRequest changeProductRequest) {
    productService.updateProduct(id, changeProductRequest);
  }

  @DeleteMapping("/{id}")
  public void deleteProduct(@PathVariable int id) {
    productService.deleteProduct(id);
  }


  @PostMapping("/purchase")
  @ResponseBody
  public PurchaseProductResponse purchaseProduct(@Valid @RequestBody List<Product> products) {
    PurchaseProductResponse purchaseProductResponse = new PurchaseProductResponse();
    productService.purchaseProduct(products);
    return purchaseProductResponse;
  }

  @PostMapping("/unpurchase")
  public void unpurchaseProduct(@Valid @RequestBody List<PurchasedProduct> products) {
    productService.unpurchaseProduct(products);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(HttpClientErrorException.BadRequest.class)
  public Map<String, String> handleValidationExceptions(
      HttpClientErrorException.BadRequest ex) {
    Map<String, String> errors = new HashMap<>();
    errors.put("message", ex.getMessage());
    return errors;
  }
}
