package com.nab.assignment.repositoryservice.repository;

import com.nab.assignment.repositoryservice.entity.Product;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {

  @Override
  Optional<Product> findById(Integer aInteger);

  @Override
  List<Product> findAll(Specification<Product> specification);

  @Override
  <S extends Product> S save(S s);

  @Override
  void deleteById(Integer aInteger);
}
