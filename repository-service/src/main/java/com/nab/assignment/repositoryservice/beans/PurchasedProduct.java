package com.nab.assignment.repositoryservice.beans;

import lombok.Data;

@Data
public class PurchasedProduct {
  private int id;
  private int quantity;
  private OrderStatus orderStatus;

  public PurchasedProduct(int productId, int quantity) {
    this.id = productId;
    this.quantity = quantity;
  }
}
