package com.nab.assignment.repositoryservice.service;

import com.nab.assignment.repositoryservice.beans.ChangeProductRequest;
import com.nab.assignment.repositoryservice.beans.PurchasedProduct;
import com.nab.assignment.repositoryservice.entity.Product;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProductService {

  void createProduct(ChangeProductRequest changeProductRequest);

  Product getProduct(int id);

  List<Product> getProducts(String search, String sort, Sort.Direction direction);

  void updateProduct(int id, ChangeProductRequest changeProductRequest);

  void deleteProduct(int id);

  List<PurchasedProduct> purchaseProduct(List<Product> products);

  void unpurchaseProduct(List<PurchasedProduct> products);

}
