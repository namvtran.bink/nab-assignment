package com.nab.assignment.purchaseservice.service.impl;

import com.nab.assignment.purchaseservice.beans.CustomerRequest;
import com.nab.assignment.purchaseservice.builder.CustomerBuilder;
import com.nab.assignment.purchaseservice.builder.CustomerSpecificationBuilder;
import com.nab.assignment.purchaseservice.entity.Customer;
import com.nab.assignment.purchaseservice.repository.CustomerRepository;
import com.nab.assignment.purchaseservice.service.CustomerService;
import com.nab.assignment.purchaseservice.validator.CustomerFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private CustomerFieldValidator customerFieldValidator;

  @Override
  public void createCustomer(CustomerRequest CustomerRequest) {
    Customer Customer = buildCustomer(new CustomerBuilder(), CustomerRequest);
    customerRepository.save(Customer);
  }

  @Override
  public Customer getCustomer(int id) {
    return customerRepository.findById(id).orElse(null);
  }

  @Override
  public List<Customer> getCustomers(String search, String sortField, Sort.Direction direction) {
    sortField = Optional.ofNullable(sortField).orElse("name");
    customerFieldValidator.validate(sortField);

    direction = Optional.ofNullable(direction).orElse(Sort.Direction.ASC);

    Specification<Customer> spec = getCustomerSpecification(search);

    return customerRepository.findAll(spec, Sort.by(direction, sortField));
  }

  @Override
  public void updateCustomer(int id, CustomerRequest CustomerRequest) {
    Customer customer = customerRepository.findById(id).orElseThrow(() -> {
      String message = String.format("Customer with id: %d is not exists to update", id);
      return HttpClientErrorException.create(message, HttpStatus.BAD_REQUEST,
          HttpStatus.BAD_REQUEST.getReasonPhrase(), null, null, null);
    });
    customer = buildCustomer(new CustomerBuilder(customer), CustomerRequest);
    customerRepository.save(customer);
  }

  @Override
  public void deleteCustomer(int id) {
    customerRepository.deleteById(id);
  }

  private Specification<Customer> getCustomerSpecification(String search) {
    CustomerSpecificationBuilder builder = new CustomerSpecificationBuilder();
    Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
    Matcher matcher = pattern.matcher(search + ",");
    while (matcher.find()) {
      customerFieldValidator.validate(matcher.group(1));
      builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
    }

    return builder.build();
  }

  private Customer buildCustomer(CustomerBuilder CustomerBuilder, CustomerRequest CustomerRequest) {
    return CustomerBuilder
        .setName(CustomerRequest.getName())
        .setPhoneNumber(CustomerRequest.getPhoneNumber())
        .setEmail(CustomerRequest.getEmail())
        .setAddress(CustomerRequest.getAddress())
        .build();
  }
}
