package com.nab.assignment.purchaseservice.service;

import com.nab.assignment.purchaseservice.beans.CustomerRequest;
import com.nab.assignment.purchaseservice.entity.Customer;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface CustomerService {

  void createCustomer(CustomerRequest changeCustomerRequest);

  Customer getCustomer(int id);

  List<Customer> getCustomers(String search, String sort, Sort.Direction direction);

  void updateCustomer(int id, CustomerRequest changeCustomerRequest);

  void deleteCustomer(int id);

}
