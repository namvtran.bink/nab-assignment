package com.nab.assignment.purchaseservice.builder;

import com.nab.assignment.purchaseservice.entity.Customer;

public class CustomerBuilder {
  private Customer customer;
  
  public CustomerBuilder() {
    customer = new Customer();
  }


  public CustomerBuilder(Customer customer) {
    this.customer = customer;
  }

  public CustomerBuilder setId(int id) {
    customer.setId(id);
    return this;
  }

  public CustomerBuilder setPhoneNumber(String brand) {
    customer.setPhoneNumber(brand);
    return this;
  }

  public CustomerBuilder setEmail(String imageUrl) {
    customer.setEmail(imageUrl);
    return this;
  }

  public CustomerBuilder setName(String name) {
    customer.setName(name);
    return this;
  }

  public CustomerBuilder setAddress(String price) {
    customer.setAddress(price);
    return this;
  }
  
  public Customer build() {
    return customer;
  }
}
