package com.nab.assignment.purchaseservice.controller;

import com.nab.assignment.purchaseservice.beans.CustomerRequest;
import com.nab.assignment.purchaseservice.entity.Customer;
import com.nab.assignment.purchaseservice.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customers")
public class CustomerController {

  @Autowired
  private CustomerService customerService;

  @GetMapping("/{id}")
  public Customer getCustomer(@PathVariable int id) {
    return customerService.getCustomer(id);
  }

  @GetMapping
  @ResponseBody
  public List<Customer> getCustomers(@RequestParam(required = false) String search,
                                   @RequestParam(required = false) String sort,
                                   @RequestParam(required = false) Sort.Direction direction) {
    return customerService.getCustomers(search, sort, direction);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public void createCustomer(@Valid @RequestBody CustomerRequest changeCustomerRequest) {
    customerService.createCustomer(changeCustomerRequest);
  }

  @PutMapping("/{id}")
  public void updateCustomer(@PathVariable int id,
                            @Valid @RequestBody CustomerRequest changeCustomerRequest) {
    customerService.updateCustomer(id, changeCustomerRequest);
  }

  @DeleteMapping("/{id}")
  public void deleteCustomer(@PathVariable int id) {
    customerService.deleteCustomer(id);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(HttpClientErrorException.BadRequest.class)
  public Map<String, String> handleValidationExceptions(
      HttpClientErrorException.BadRequest ex) {
    Map<String, String> errors = new HashMap<>();
    errors.put("message", ex.getMessage());
    return errors;
  }
}
