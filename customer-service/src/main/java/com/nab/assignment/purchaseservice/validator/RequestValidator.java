package com.nab.assignment.purchaseservice.validator;

import org.springframework.web.util.HtmlUtils;

import javax.validation.ConstraintDeclarationException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;


public class RequestValidator implements ConstraintValidator<ValidRequest, Object> {

  private static final String[] htmlWhiteList = {"'", "&"};

  @Override
  public boolean isValid(Object value, ConstraintValidatorContext context) {
    try {
      validate(value);
      return true;
    } catch (Exception e) {
      throw new ConstraintDeclarationException(e.getMessage());
    }
  }

  private static void validate(Object object)
      throws IllegalAccessException {
    if (object instanceof String || object instanceof Character) {
      validateString(String.valueOf(object));
    } else if (!(object instanceof Number)) {
      recursiveValidation(object);
    }
  }

  /**
   * Iterates over all the fields of the received object and, if it is a String, it validates it
   *
   * @param object
   * @throws IllegalAccessException
   */
  private static void recursiveValidation(Object object)
      throws IllegalAccessException {

    Field[] fields = getAllFields(object);
    for (Field f : fields) {
      f.setAccessible(true);
      Object v = f.get(object);
      if (v instanceof String) {
        validateString((String) v);
      } else if (v instanceof Collection) {
        for (Object o : (Collection<?>) v) {
          validate(o);
        }
      } else if (v instanceof Map) {
        for (Object o : ((Map<?, ?>) v).keySet()) {
          validate(o);
        }
        for (Object o : ((Map<?, ?>) v).values()) {
          validate(o);
        }
      } else if (v != null) {
        validate(v);
      }
    }
  }

  private static Field[] getAllFields(Object object) {
    return object.getClass().getDeclaredFields();
  }

  /**
   * Validates if the parameter passed contains suspicious characters. If so, throw Exception
   *
   * @param value String value to validate
   */
  private static void validateString(String value) {
    value = charWhitelistReplace(value);
    if (!HtmlUtils.htmlEscape(value).equalsIgnoreCase(value) || value.contains("�")) {
      throw new IllegalArgumentException(value);
    }
  }

  /**
   * Replace any occurrence of white listed character list
   *
   * @param value
   * @return String the cleaned value
   */
  private static String charWhitelistReplace(String value) {
    for (String whiteListItem : htmlWhiteList) {
      value = value.replaceAll(whiteListItem, " ");
    }
    return value;
  }
}
