package com.nab.assignment.purchaseservice.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = RequestValidator.class)
@Documented
public @interface ValidRequest {

  String message() default "{ValidInput}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
