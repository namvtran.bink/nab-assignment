package com.nab.assignment.purchaseservice.beans;

import com.nab.assignment.purchaseservice.validator.ValidRequest;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@ValidRequest
public class CustomerRequest implements Serializable {
  private static final long serialVersionUID = 5398254779477376977L;

  @NotEmpty
  private String name;
  @NotEmpty
  private String phoneNumber;
  @Email
  @NotEmpty
  private String email;
  @NotEmpty
  private String address;
}
