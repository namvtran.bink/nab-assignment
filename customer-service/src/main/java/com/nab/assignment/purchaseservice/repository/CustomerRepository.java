package com.nab.assignment.purchaseservice.repository;

import com.nab.assignment.purchaseservice.entity.Customer;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Integer>, JpaSpecificationExecutor<Customer> {

  @Override
  Optional<Customer> findById(Integer aLong);

  @Override
  List<Customer> findAll(Specification<Customer> specification);

  @Override
  <S extends Customer> S save(S s);

  @Override
  void deleteById(Integer aLong);
}
